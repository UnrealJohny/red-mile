﻿using UnityEngine;
using System.Collections;

public class TorchComponent : MonoBehaviour
{
	private TorchesController torchController;

	public Light torchLight;

	void Awake ()
	{
		torchController = GameObject.FindGameObjectWithTag ("TorchController").GetComponent<TorchesController> ();
	}

	void OnTriggerEnter ()
	{
		torchController.EnterTrigger (true, this);
	}

	void OnTriggerExit ()
	{
		torchController.EnterTrigger (false, this);

	}

	/*void OnTriggerStay(){

		torchController.KillTimer (0, 0);

	}*/

	public void EnableTorch (bool enable)
	{

		torchLight.enabled = enable;
	}
}
