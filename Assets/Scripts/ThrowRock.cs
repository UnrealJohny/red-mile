﻿using UnityEngine;
using System.Collections;

public class ThrowRock : MonoBehaviour
{

	// Use this for initialization
	public float range;
	public int totalskulls;

	public GameObject skullPrefab;
	public GameObject skullInstanttiator;

	public float force;
		
	private RaycastHit hit;
	private Ray ray;

	void Start(){

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

	}
		
	void Update ()
	{
		// If the Button “Fire1” is pressed down it will return true and so we enter the if condition
		if (Input.GetButtonDown ("Fire1") && totalskulls >= 1) {
			totalskulls--;
			/*// Whatever is inside will only be called once as soon as the Button “Fire1” is pressed down
				
			// The Vector2 class holds the position for a point with only x and y coordinates
			// The center of the screen is calculated by dividing the width and height by half
			Vector2 screenCenterPoint = new Vector2 (Screen.width / 2, Screen.height / 2);
				
			// The method ScreenPointToRay needs to be called from a camera
			// Since we are using the MainCamera of our scene we can have access to it using the Camera.main
			ray = Camera.main.ScreenPointToRay (screenCenterPoint);
				
			if (Physics.Raycast (ray, out hit, range)) {
				// A collision was detected please deal with it
				if (hit.collider.gameObject.tag == "Inimigo") {	
					Debug.Log (hit.collider.gameObject);
					hit.collider.gameObject.SetActive (false);
				}

			}*/

			GameObject instantce = (GameObject)GameObject.Instantiate (skullPrefab, skullInstanttiator.transform.position, Quaternion.identity);
			instantce.GetComponent<Rigidbody>().AddForce (transform.forward * force, ForceMode.Impulse);
				
		}
	}

	void OnDrawGizmosSelected ()
	{
		Gizmos.DrawWireSphere (transform.position, range);
	}
}
