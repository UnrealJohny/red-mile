﻿using UnityEngine;
using System.Collections;

public class EnemyAIController : MonoBehaviour
{

	public enum EnemyState
	{

		PATROL,
		CHASE,
		SEARCH,
		ATTACK

	}

	private EnemyState enemyState;

	public GameObject		waypointsParent;
	private GameObject[]	waypoints;

	public float distanceSight;
	public float angleSight;

	public float speedPatrol;
	public float speedChase;

	private GameObject player;

	private NavMeshAgent agent;

	private const float MIN_DISTANCE_REACH = 1.0f;

	void Awake ()
	{

		agent = GetComponent<NavMeshAgent> ();

		player = GameObject.FindGameObjectWithTag ("Player");

	}

	void Start ()
	{
		waypoints = new GameObject[waypointsParent.transform.childCount];
		for (int i = 0; i < waypointsParent.transform.childCount; i++) {
			waypoints [i] = waypointsParent.transform.GetChild (i).gameObject;
		}

		SetState (EnemyState.PATROL);

	}

	void Update ()
	{

		switch (enemyState) {

		case EnemyState.PATROL:
			UpdatePATROL ();
			break;

		case EnemyState.CHASE:
			UpdateCHASE ();
			break;

		case EnemyState.SEARCH:
			UpdateSEARCH ();
			break;

		case EnemyState.ATTACK:
			UpdateATTACK ();
			break;

		}

	}

	private void SetState (EnemyState state)
	{
		switch (state) {
			
		case EnemyState.PATROL:
			agent.speed = speedPatrol;
			agent.destination = GetNextWaypoint ();
			break;
			
		case EnemyState.CHASE:
			agent.speed = speedChase;
			agent.destination = player.transform.position;
			break;
			
		case EnemyState.SEARCH:

			break;
			
		case EnemyState.ATTACK:

			break;
			
		}
	}

	private void UpdatePATROL ()
	{
		if (IsPlayerWithinRange () && IsPlayerInSight ()) {
			SetState (EnemyState.CHASE);
		} else {

			if (Vector3.Distance (transform.position, agent.destination) < MIN_DISTANCE_REACH) {
				agent.destination = GetNextWaypoint ();
			}
		}
	}

	private void UpdateCHASE ()
	{
		agent.destination = player.transform.position;
		if (!IsPlayerWithinRange ()) {
			SetState (EnemyState.PATROL);
		}
	}

	private void UpdateSEARCH ()
	{
		
	}

	private void UpdateATTACK ()
	{
		
	}

	private Vector3 GetNextWaypoint ()
	{
		var rnd = Random.Range (0, waypoints.Length);
		return waypoints [rnd].transform.position;
	}

	private bool IsPlayerWithinRange ()
	{

		bool result = false;

		if (Vector3.Distance (transform.position, player.transform.position) < distanceSight) {
			result = true;
		}

		return result;
	}

	private bool IsPlayerInSight ()
	{
		bool result = false;

		Vector3 orientation = player.transform.position - transform.position;

		if (Vector3.Dot (transform.forward.normalized, orientation.normalized) > angleSight) {

			RaycastHit hit;

			Ray ray = new Ray (transform.position, orientation);

			if (Physics.Raycast (ray, out hit, distanceSight)) {
				if (hit.transform.gameObject == player) {
					result = true;

					Debug.Log ("Player In Sight");
				}

			}

		}
		return result;
	}

	private void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere (transform.position, distanceSight);
	}
}
