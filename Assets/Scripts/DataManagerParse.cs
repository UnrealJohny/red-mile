﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Collections.Generic;

public class DataManagerParse : MonoBehaviour {

	private const string OBJECT_NAME = "Player";
	private const string KEY_PLAYERNAME = "playerName";
	private const string KEY_TIME = "time";
	List<Score> scores;

	public UIController uiController;

	void Start(){

		//StartCoroutine(CoRetrieveScore());

	}

	public void SubmitScore(Score score){

		StartCoroutine(CoSubmitScore(score));


	}

	private IEnumerator CoSubmitScore(Score score){

		ParseObject scoreObject = new ParseObject(OBJECT_NAME);

		scoreObject[KEY_PLAYERNAME] = score.GetPlayerName();
		scoreObject[KEY_TIME] = score.GetTime();

		var saveTask = scoreObject.SaveAsync();

		while(!saveTask.IsCompleted){

			yield return null;

		}

		//StartCoroutine(CoRetrieveScore());

	}

	/*private IEnumerator CoRetrieveScore(){

		ParseQuery<ParseObject> query = ParseObject.GetQuery(OBJECT_NAME)
			.Limit(10)
			.OrderBy(KEY_TIME);

		var queryTask = query.FindAsync();

		while(!queryTask.IsCompleted){

			yield return null;

		}

		scores = new List<Score>();

		IEnumerable<ParseObject> results = queryTask.Result;

		foreach(ParseObject parseObject in results){

			Score score = new Score(parseObject.Get<string>(KEY_PLAYERNAME), parseObject.Get<float>(KEY_TIME), parseObject.Get<float>(KEY_ACCURACY));

			scores.Add(score);

		}

		uiController.UpdateTable();

	}*/
	public List<Score> GetScores(){

		return scores;

	}

}
