﻿using UnityEngine;
using System.Collections;

public class TriggerMorte : MonoBehaviour {
	
	private static float initialTime;
	private static float totalTime;
	private static bool conta = false;

	public GameObject Trigger;

	void OnTriggerEnter(Collider other){

		Debug.Log ("Entrei");
		conta = false;
		totalTime = 0;
		initialTime = 0;

	}

	void OnTriggerStay(Collider other){
		
		Debug.Log ("Estou");
		conta = false;
		totalTime = 0;
		initialTime = 0;
		

	}

	void OnTriggerExit(Collider other){

		Debug.Log("Sai");
		conta = true;
		initialTime = Time.time;

	}

	void Update (){

		totalTime = Time.time - initialTime;

		if (totalTime >= 10 && conta) {
			
			Debug.Log("Game Over");
			
		}
		
	}
}
