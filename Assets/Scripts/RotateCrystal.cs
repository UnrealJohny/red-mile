﻿using UnityEngine;
using System.Collections;

public class RotateCrystal : MonoBehaviour {

	new Vector3 posiçao;

	// Use this for initialization
	void Start () {
		posiçao = gameObject.transform.position;
	}

	// Update is called once per frame
	void Update () {
		this.transform.RotateAround(posiçao, Vector3.up, 20 * Time.deltaTime);
	}
}
