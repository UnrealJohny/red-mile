﻿using UnityEngine;
using System.Collections;

public class Refillskull : MonoBehaviour
{

	public int total;
	public int RefillAmmo;
	public bool refilActive;

	void OnTriggerStay (Collider other)
	{
		if (other.tag == "Player" && refilActive) {
			if (RefillAmmo > 0) {
				RefillAmmo--;
				other.gameObject.GetComponentInChildren<ThrowRock> ().totalskulls = total;
				refilActive = false;
			}
		}

	}

	void OnTriggerExit (Collider other){

		refilActive = true;

	}
}
