﻿using UnityEngine;
using System.Collections;

public class Score {

	private string playerName;
	private float time;

	public Score(string playerName, float time){

		this.playerName = playerName;
		this.time = time;

	}

	public string GetPlayerName(){


		return this.playerName;
	}

	public float GetTime(){


		return this.time;
	}

}