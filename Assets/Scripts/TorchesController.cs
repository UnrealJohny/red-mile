﻿using UnityEngine;
using System.Collections;

public class TorchesController : MonoBehaviour
{
	public GameObject player;
	public TorchComponent[] torches;
	public TorchComponent currentTorch;

	public GameController gameController;

	public float currentTime;


	void Awake ()
	{
		GameObject[] torchesObject = GameObject.FindGameObjectsWithTag ("Torch");

		torches = new TorchComponent[torchesObject.Length];

		for (int i = 0; i < torchesObject.Length; i++) {
			torches [i] = torchesObject [i].GetComponent<TorchComponent> ();
		}
	}

	/*public void KillTimer (float initTime, float totTime){

		initialTime = initTime;
		totalTime = totTime;
	
	}*/

	public void EnterTrigger (bool enter, TorchComponent torch)
	{
		if (enter) {
			currentTorch = torch;
		}
		else {
			currentTorch = null;
		}
	}

	void Update ()
	{
		if (currentTorch) {
			if (Input.GetKeyUp (KeyCode.E)) {
				EnableTorch ();
			}
		}


		if (currentTorch != null) {

			if (!currentTorch.torchLight.enabled) {
				Timer ();
			}
			else {
				currentTime = 0;
			}
		}
		else {
			Timer ();
		}
	}

	private void Timer ()
	{
		currentTime += Time.deltaTime;

		if (currentTime >= 10) {
			//Application.LoadLevel ("GameOver");
			gameController.MorreuPorTempo();

		}
	}
	
	private void EnableTorch ()
	{

		for (int i = 0; i < torches.Length; i++) {

			if (torches [i] == currentTorch) {
				currentTorch.EnableTorch (true);
			}
			else {

				torches [i].EnableTorch (false);

			}

		}

	}
}
