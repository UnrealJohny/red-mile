﻿using UnityEngine;
using System.Collections;

public class MenuExit : MonoBehaviour
{
	
	public Texture2D normalTexture;
	public Texture2D hoverTexture;
	public Texture2D pressedTexture;
	
	void OnMouseEnter ()
	{
		GetComponent<Renderer>().material.mainTexture = hoverTexture;
	}
	
	void OnMouseExit ()
	{
		GetComponent<Renderer>().material.mainTexture = normalTexture;
	}
	
	void OnMouseDown ()
	{
		GetComponent<Renderer>().material.mainTexture = pressedTexture;
	}
	
	void OnMouseUp ()
	{
		GetComponent<Renderer>().material.mainTexture = normalTexture;
		
		Application.Quit ();
	}
	
}