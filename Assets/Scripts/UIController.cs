﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIController : MonoBehaviour {

	public InputField inputName;
	public Text time;

	public DataManagerParse dataManager;

	/*public VerticalLayoutGroup grid;
	public GameObject row;*/

	public void SubmitData(){

		Score score = new Score (inputName.text, float.Parse (time.text));

		dataManager.SubmitScore(score);

		Application.LoadLevel("Menu");

	}

	/*public void UpdateTable(){


		List<Score> scores = dataManager.GetScores();


		for (int i = 0; i<scores.Count; i++){

			Row rowInstance = null;

			if(i< grid.transform.childCount){

				rowInstance = grid.transform.GetChild(i).GetComponent<Row>();

			}else{

			GameObject instance = (GameObject)Instantiate(row, Vector3.zero, Quaternion.identity);

			instance.transform.SetParent (grid.transform);
				
			rowInstance = instance.GetComponent<Row>();

			}
			rowInstance.textName.text = scores[i].GetPlayerName();
			rowInstance.textTime.text = scores[i].GetTime().ToString();
			rowInstance.textAccuracy.text = scores[i].GetAccuracy().ToString();
			
		}
	}*/

	public void ExitGame(){

		Application.Quit();

	}
}

